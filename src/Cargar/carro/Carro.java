package Cargar.carro;

import java.util.HashMap;
import java.util.Map;

import Carga.id.com.Idcarro;
import Carga.id.com.Idjuego;


public class Carro {
	 protected Conductor conductor;
	    protected Integer distancia;
	    protected Idjuego idjuego;
	    private final Map<Idcarro, Conductor> carros = new HashMap<>();

	    public Carro() {
	    }

	    public Carro(Conductor conductor, Integer distancia,  Idjuego idjuego) {
	        this.conductor = conductor;
	        this.distancia = distancia;
	        this.idjuego = idjuego;
	    }

	    public void asignarConductor(Idcarro Idcarro, Conductor conductor) {
	        carros.put(Idcarro, conductor);

	    }

	    public Map<Idcarro, Conductor> carros() {
	        return carros;

	    }

	    public void setDistancia(Integer distancia) {
	        this.distancia = distancia;
	    }

	    public Integer numeroCarros() {
	        return carros.size();
	    }

	    public Conductor conductor() {

	        return conductor;

	    }

	    public Integer distancia() {

	        return distancia;

	    }

	    

}
